function createNewUser() {
  const name = prompt("your name", "");
  const surname = prompt("your surname", "");
  const birthString = prompt("You birthday dd.mm.yyyy.");

  return {
    firstName: name,
    lastName: surname,
    birthday: birthString,
    getLogin() {
      return (
        this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase()
      );
    },
    getAge: function () {
      let now = new Date();
      let birthday = this.birthday.split(".");
      let birthdayData = new Date(birthday[2], birthday[1] - 1, birthday[0]);
      let age = now.getFullYear() - birthdayData.getFullYear();
      if (
        now.getMonth() < birthdayData.getMonth() ||
        (now.getMonth() == birthdayData.getMonth() &&
          now.getDate() < birthdayData.getDate())
      ) {
        age--;
      }

      return age;
    },
    getPassword: function () {
      return (
        this.firstName.charAt(0).toUpperCase() +
        this.lastName.toLowerCase() +
        this.birthday.slice(-4)
      );
    },
  };
}
const user = createNewUser();
console.log(`your full name : ${user.getLogin()}`);
console.log(`your full date: ${user.getAge()}`);
console.log(`your full name and year: ${user.getPassword()}`);

// Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
// єкранування це коли ми хочемо знайти якись один символ в стрічці . за допомогою \
// також за допомогою слеша ми можемо не тільки шукати спеціальні сиволи (точки , запяті , плюси та мінуси)
// ми також можемо додавати ці ж символи , наприклад коли нам потрібно вставити кавички чи якийсь знак ,
// то перед цим знаком ми також ставимо \ .
// Які засоби оголошення функцій ви знаєте?
// Function declaration - функция объявляется с помощью ключевого слова function
// Function expression - функция также объявляется с помощью ключевого слова function, но у нее нет имени, и она записывется в переменную.
// стрілскова функція
// Що таке hoisting, як він працює для змінних та функцій?
// на этапе создания, движок JavaScript просматривает код и, как только он видит ключевое слово var или ключевое слово function, он выделяет некоторую память для них.
// Поднятие предполагает, что объявления переменных var и функций function физически перемещаются в начало кода, но, на самом деле это не так.
