function arr(city, element = document.body) {
  return element.insertAdjacentHTML(
    "afterbegin",
    `<ul>${city.map((element) => `<li>${element}</li>`).join("")}</ul>`
  );
}
arr(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);

// Опишіть, як можна створити новий HTML тег на сторінці.
// методом document.createElement('div')
// Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
// перший параметр функції insertAdjacentHTML це куди ми хочемо вставити елемент в документі ,
// "beforebegin" – вставить html непосредственно перед elem,
// "afterbegin" – вставить html в начало elem,
// "beforeend" – вставить html в конец elem,
// "afterend" – вставить html непосредственно после elem.
// Як можна видалити елемент зі сторінки?
// методом remove()
